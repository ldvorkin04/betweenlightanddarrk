using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public float speed;
    public Transform[] points;
    public int i;
    private bool facingRight;
    // Start is called before the first frame update
    private void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, points[i].position, speed * Time.fixedDeltaTime);
        if(Vector2.Distance(transform.position, points[i].position ) < 0.2f)
        {
            if(i>0)
            {
                i = 0;
                Flip();
            }
            else
            {
                i = 1;
                Flip();
            }

        }
    }
    void Flip()
    {
        facingRight = !facingRight!;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
}
