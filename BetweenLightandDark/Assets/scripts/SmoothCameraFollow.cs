using UnityEngine;

public class SmoothCameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed = 0.125f;
    public float distance = 5f;
    private Vector3 velocity = Vector3.zero;

    void Update()
    {
        if (target)
        {
            Vector3 targetPosition = target.position;
            targetPosition.y = transform.position.y;
            targetPosition.z -= distance;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothSpeed);
        }
    }
}